" 普通的設置{{{
set nocompatible                     " 不要兼容vi
filetype off                         " 必须的设置：
syntax enable                        " 打开语法高亮
set ai                               " 设置自动缩进
set guifont=Monospace\ 13            " 设置字的大小
set scrolloff=3                      " 滚动时到顶或底端距离多少行开始滚动屏幕
set cmdheight=2                      " 命令行的高度
set cursorline                       " 设置光标高亮显示
set smartindent                      " 智能自动缩进
set showmatch                        " 显示括号配对情况
set mouse=a                          " 启用鼠标set ruler
set ruler                            " 显示光标的位置，所有的时间
set magic							 " 设置魔术"
set whichwrap+=<,>,h,l				 " 允许backspace和光标键跨越行边界"
set incsearch                        " 查找book时，当输入/b时会自动找到
set hlsearch                         " 开启高亮显示结果
set nowrapscan                       " 搜索到文件两端时不重新搜索
set vb t_vb=                         " 关闭错误提示音
set hidden                           " 允许在有未保存的修改时切换缓冲区
set list                             " 显示Tab符，使用一高亮竖线代替
set listchars=tab:\¦\ ,trail:+,eol:¬ " 设置特殊的字符
set smarttab                         " 一次backspace四个空格键
set backspace=indent,eol,start       " 允许通过在插入模式下的一切退格
set history=50                       " 命令行保存的历史记录条数
set showcmd                          " 显示命令行
set viminfo='1000,f1,<500            " viminfo文件的设置
set writebackup                      " Make a backup before overwriting a file
set nobackup                         " 设置无备份文件
set wrap                             " 设置不自动换行
set foldlevel=99                     " 禁止自动折叠
set autoread                         " 当文件在外部改变时会自动重新加载
set undofile                         " 可以无限地撤消操作
set undodir=~/.vim/undodir           " 无限撤消时的文件存放位置
set undolevels=999                   " 无限撤消的次数
set textwidth=80                     " 文本的宽度,如果不设置不自动换行，超过这个长度会自动换行
"set colorcolumn=85                   " 设置列高亮显示
set fileencoding=utf-8
set nu
set tabstop=4
set shiftwidth=4
set cindent shiftwidth=4
set wildmenu
set wildmode =list:longest
"gvim打开支持的文件编码
set fileencodings=utf-8,gbk,cp936,gb2312,big5,evc-jp,evc-kr,latin1,ucs-bom

"}}}
"使用移动窗口更方便"{{{
map <C-J> <C-W>j<C-W>_
map <C-K> <C-W>k<C-W>_
map <C-L> <C-W>l<C-W>_
map <C-H> <C-W>h<C-W>_
map <s-tab> :bp<cr>
map <tab> :bn<cr>
map ,f :q!<CR>
map ee :e ~/.vimrc<CR>
nnoremap j gj
nnoremap k gk
map <C-S> <ESC>:w<CR>
imap <C-S> <ESC>:w<CR>a
vmap <C-S> <ESC>:w<CR>
nmap fi :!firefox %.html & <CR><CR>
"}}}
"用空格键来开关折叠（说明西方“"”后面的内容为注释，不会被VIM所识别）{{{
set foldenable
nnoremap <space> @=((foldclosed(line('.')) < 0) ? 'zc' : 'zo')<CR>
"设定自动保存折叠
  autocmd BufWinLeave * if expand('%') != '' && &buftype == '' | mkview | endif
  autocmd BufRead     * if expand('%') != '' && &buftype == '' | silent loadview | syntax on | endif
"}}}
" statusline {{{
if has('statusline')
	set laststatus=2
	" Broken down into easily includeable segments
	set statusline=%<%f\   " Filename
	set statusline+=%w%h%m%r " Options
	"set statusline+=%{fugitive#statusline()} "  Git Hotness
	set statusline+=\ [%{&ff}/%Y]            " filetype
	set statusline+=\ [%{getcwd()}]          " current dir
	"set statusline+=\ [A=\%03.3b/H=\%02.2B] " ASCII / Hexadecimal value of char
	set statusline+=\ [%{(&fenc==\"\"?&enc:&fenc).(&bomb?\",BOM\":\"\")}]
	set statusline+=%=%-14.(%l,%c%V%)\ %p%%  " Right aligned file nav info
endif 
"}}}
"修改leader键为逗号{{{
let mapleader=","

"使用<leader>空格来取消搜索高亮
nnoremap <leader><space> :noh<cr>
"取消粘贴缩进
nmap <leader>p :set paste<CR>
nmap <leader>pp :set nopaste<CR>
"文件类型切换
nmap <leader>fj :set ft=javascript<CR>
nmap <leader>fc :set ft=css<CR>
nmap <leader>fx :set ft=xml<CR>
nmap <leader>fm :set ft=mako<CR>
"}}}
" au的一些设置{{{
"au FileType php setlocal dict+=~/.vim/dict/php_funclist.dict
"au FileType css setlocal dict+=~/.vim/dict/css.dict
"au FileType c setlocal dict+=~/.vim/dict/c.dict
"au FileType cpp setlocal dict+=~/.vim/dict/cpp.dict
"au FileType scale setlocal dict+=~/.vim/dict/scale.dict
"au FileType javascript setlocal dict+=~/.vim/dict/javascript.dict
"au FileType html setlocal dict+=~/.vim/dict/javascript.dict
"au FileType html setlocal dict+=~/.vim/dict/css.dict
au BufRead,BufNewFile *.{md,mdown,mkd,mkdn,markdown,mdwn}   set filetype=mkd
au BufRead,BufNewFile *.{go}   set filetype=go
au BufRead,BufNewFile *.{js}   set filetype=javascript
"}}}
"Vundle Settings {{{
set rtp+=~/.vim/bundle/vundle/
call vundle#rc()
" let Vundle manage Vundle
" required!
Bundle 'gmarik/vundle'
Bundle 'matchit.zip'
Bundle "ack.vim"
Bundle 'tpope/vim-surround'
Bundle 'scrooloose/nerdcommenter'
Bundle 'git4programer/vim-multiple-cursors'
Bundle 'git4programer/tabular'
Bundle 'taglist.vim'
Bundle 'winmanager'
Bundle 'sudo.vim'
Bundle 'superSnipMate'
Bundle 'brookhong/DBGPavim'
Bundle 'ctrlp.vim'
Bundle 'python.vim'
Bundle 'sourcebeautify.vim'
Bundle 'OmniCppComplete'
Bundle 'xml.vim'
Bundle 'Python-Syntax'
Bundle 'AutoClose'
Bundle 'jsruntime.vim'
Bundle 'jsoncodecs.vim'
Bundle 'minibufexpl.vim'
Bundle 'Markdown'
Bundle 'Markdown-syntax'
Bundle 'jnwhiteh/vim-golang'
Bundle 'AutoComplPop'
Bundle 'gitdiff.vim'
Bundle 'asins/vimcdoc'
if has("python" || has("python3"))
	Bundle 'Lokaltog/powerline',{'rtp','/powerline/bindings/vim'}
	let airline=0
else
	Bundle 'bling/vim-airline'
	let airline=1
endif

"}}}
"快速輸入時間 ""{{{
ia xdate <c-r>=strftime("%Y-%m-%d")<cr>
ia xdtime <c-r>=strftime("%Y-%m-%d %H:%M:%S")<cr>
"}}}
"导入外部的配置文件{{{
source ~/.vim/vimrc.bundles
source ~/.vim/compileconde.vim
"}}}
" Plugins {"{{{
"emmet{{{
Bundle 'git4programer/emmet-vim'
let g:user_emmet_settings={
    \ 'indentation':' ',
	\ 'php' : {
	\ 'extends' : 'html',
	\ 'filters' : 'c',
	\ },
	\ 'xml' : {
	\ 'exists' : 'html',
	\ },
    \ 'perl':{
    \  'aliases':{
    \  'req':'require'
    \ },
    \ 'snippets':{
    \ 'use':"use strict\nuse warnings\n\n",
    \ 'warn':"warn \"|\";",
    \  }
    \ }
    \}
let g:user_emmet_expandabbr_key='<c-z>'
let g:user_emmet_complete_tag=1
"}}}
"The-NERD-Commenter{{{
Bundle 'The-NERD-Commenter'
let NERDShutUp=1
let g:snips_author='kason'
let g:vimrc_email='709392650@qq.com'
let g:vimrc_homepage='http://bnblogs.com/'
nmap <F4> :AuthorInfoDetect<cr>
"}}}
""CtrlP{{{
let g:ctrlp_map = ',t'
let g:ctrlp_open_multiple_files = 'v'
let g:ctrlp_by_filename = 1

set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*.pyc,*.png,*.jpg,*.gif
let g:ctrlp_custom_ignore = {
  \ 'dir':  '\v[\/]\.(git|hg|svn)$',
  \ 'file': '\v\.(log|jpg|png|jpeg)$',
  \ }
"}}}
"miniBufexpl"{{{
let g:miniBufExplMapWindowNavVim = 1 
let g:miniBufExplMapWindowNavArrows = 1 
let g:miniBufExplMapCTabSwitchBufs = 1 
let g:miniBufExplModSelTarget = 1
"}}}
" taglist {{{
let Tlist_Use_Right_Window=1
"}}}
    " PIV {{{
        let g:DisableAutoPHPFolding = 0
        let g:PIVAutoClose = 0
    "}}}
    " Misc {"{{{
        let g:NERDShutUp=1
        let b:match_ignorecase = 1
    " }"}}}
    " OmniComplete {"{{{
        if has("autocmd") && exists("+omnifunc")
            autocmd Filetype *
                \if &omnifunc == "" |
                \setlocal omnifunc=syntaxcomplete#Complete |
                \endif
        endif

        hi Pmenu  guifg=#000000 guibg=#F8F8F8 ctermfg=black ctermbg=Lightgray
        hi PmenuSbar  guifg=#8A95A7 guibg=#F8F8F8 gui=NONE ctermfg=darkcyan ctermbg=lightgray cterm=NONE
        hi PmenuThumb  guifg=#F8F8F8 guibg=#8A95A7 gui=NONE ctermfg=lightgray ctermbg=darkcyan cterm=NONE

        " Some convenient mappings
        inoremap <expr> <Esc>      pumvisible() ? "\<C-e>" : "\<Esc>"
        inoremap <expr> <CR>       pumvisible() ? "\<C-y>" : "\<CR>"
        inoremap <expr> <Down>     pumvisible() ? "\<C-n>" : "\<Down>"
        inoremap <expr> <Up>       pumvisible() ? "\<C-p>" : "\<Up>"
        inoremap <expr> <C-d>      pumvisible() ? "\<PageDown>\<C-p>\<C-n>" : "\<C-d>"
        inoremap <expr> <C-u>      pumvisible() ? "\<PageUp>\<C-p>\<C-n>" : "\<C-u>"

        " Automatically open and close the popup menu / preview window
        au CursorMovedI,InsertLeave * if pumvisible() == 0|silent! pclose|endif
        set completeopt=menu,preview,longest
    " }"}}}
    " Ctags {"{{{
        set tags=./tags;/,~/.vimtags

        " Make tags placed in .git/tags file available in all levels of a repository
        let gitroot = substitute(system('git rev-parse --show-toplevel'), '[\n\r]', '', 'g')
        if gitroot != ''
            let &tags = &tags . ',' . gitroot . '/.git/tags'
        endif
    " }"}}}
    " AutoCloseTag {"{{{
        " Make it so AutoCloseTag works for xml and xhtml files as well
        au FileType xhtml,xml ru ftplugin/html/autoclosetag.vim
        nmap <Leader>ac <Plug>ToggleAutoCloseMappings
    " }"}}}
    " NerdTree {"{{{
        map <C-e> :NERDTreeToggle<CR>:NERDTreeMirror<CR>
        map <leader>e :NERDTreeFind<CR>
        nmap <leader>nt :NERDTreeFind<CR>

        let NERDTreeShowBookmarks=1
        let NERDTreeIgnore=['\.pyc', '\~$', '\.swo$', '\.swp$', '\.git', '\.hg', '\.svn', '\.bzr']
        let NERDTreeChDirMode=0
        let NERDTreeQuitOnOpen=1
        let NERDTreeMouseMode=2
        let NERDTreeShowHidden=1
        let NERDTreeKeepTreeInNewTab=1
        let g:nerdtree_tabs_open_on_gui_startup=0
    " }"}}}
    " Tabularize {"{{{
        nmap <Leader>a& :Tabularize /&<CR>
        vmap <Leader>a& :Tabularize /&<CR>
        nmap <Leader>a= :Tabularize /=<CR>
        vmap <Leader>a= :Tabularize /=<CR>
        nmap <Leader>a: :Tabularize /:<CR>
        vmap <Leader>a: :Tabularize /:<CR>
        nmap <Leader>a:: :Tabularize /:\zs<CR>
        vmap <Leader>a:: :Tabularize /:\zs<CR>
        nmap <Leader>a, :Tabularize /,<CR>
        vmap <Leader>a, :Tabularize /,<CR>
        nmap <Leader>a,, :Tabularize /,\zs<CR>
        vmap <Leader>a,, :Tabularize /,\zs<CR>
        nmap <Leader>a<Bar> :Tabularize /<Bar><CR>
        vmap <Leader>a<Bar> :Tabularize /<Bar><CR>
    " }"}}}
    " Session List {"{{{
        set sessionoptions=blank,buffers,curdir,folds,tabpages,winsize
        nmap <leader>sl :SessionList<CR>
        nmap <leader>ss :SessionSave<CR>
        nmap <leader>sc :SessionClose<CR>
    " }"}}}
    " JSON {"{{{
        nmap <leader>jt <Esc>:%!python -m json.tool<CR><Esc>:set filetype=json<CR>
    " }"}}}
    " PyMode {"{{{
        let g:pymode_lint_checker = "pyflakes"
        let g:pymode_utils_whitespaces = 0
        let g:pymode_options = 0
    " }"}}}
    " TagBar {"{{{
        nnoremap <silent> <leader>tt :TagbarToggle<CR>

        " If using go please install the gotags program using the following
        " go install github.com/jstemmer/gotags
        " And make sure gotags is in your path
        let g:tagbar_type_go = {
            \ 'ctagstype' : 'go',
            \ 'kinds'     : [  'p:package', 'i:imports:1', 'c:constants', 'v:variables',
                \ 't:types',  'n:interfaces', 'w:fields', 'e:embedded', 'm:methods',
                \ 'r:constructor', 'f:functions' ],
            \ 'sro' : '.',
            \ 'kind2scope' : { 't' : 'ctype', 'n' : 'ntype' },
            \ 'scope2kind' : { 'ctype' : 't', 'ntype' : 'n' },
            \ 'ctagsbin'  : 'gotags',
            \ 'ctagsargs' : '-sort -silent'
            \ }
    "}"}}}
    " PythonMode {"{{{
        " Disable if python support not present
        if !has('python')
            let g:pymode = 1
        endif
    " }"}}}
    " Fugitive {"{{{
        nnoremap <silent> <leader>gs :Gstatus<CR>
        nnoremap <silent> <leader>gd :Gdiff<CR>
        nnoremap <silent> <leader>gc :Gcommit<CR>
        nnoremap <silent> <leader>gb :Gblame<CR>
        nnoremap <silent> <leader>gl :Glog<CR>
        nnoremap <silent> <leader>gp :Git push<CR>
        nnoremap <silent> <leader>gr :Gread<CR>
        nnoremap <silent> <leader>gw :Gwrite<CR>
        nnoremap <silent> <leader>ge :Gedit<CR>
        nnoremap <silent> <leader>gg :SignifyToggle<CR>
    "}"}}}
		" Enable omni completion."{{{
		autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
		autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
		autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS
		autocmd FileType python setlocal omnifunc=pythoncomplete#Complete
		autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags
		autocmd FileType ruby setlocal omnifunc=rubycomplete#Complete
		autocmd FileType haskell setlocal omnifunc=necoghc#omnifunc
		autocmd FileType php set omnifunc=phpcomplete#CompletePHP
"}}}
    " UndoTree {"{{{
        nnoremap <Leader>u :UndotreeToggle<CR>
        " If undotree is opened, it is likely one wants to interact with it.
        let g:undotree_SetFocusWhenToggle=1
    " }"}}}
    " vim-airline {"{{{
        " Set configuration options for the statusline plugin vim-airline.
        " Use the powerline theme and optionally enable powerline symbols.
        " To use the symbols , , , , , , and .in the statusline
        " segments add the following to your .vimrc.before.local file:
        "   let g:airline_powerline_fonts=1
        " If the previous symbols do not render for you then install a
        " powerline enabled font.
        let g:airline_theme = 'solarized' " 'powerlineish' is another choice
        if !exists('g:airline_powerline_fonts')
            " Use the default set of separators with a few customizations
            let g:airline_left_sep='›'  " Slightly fancier than '>'
            let g:airline_right_sep='‹' " Slightly fancier than '<'
        endif
    " }"}}}
" DBGPavim""{{{
let g:dbgPavimPort = 9001
let g:dbgPavimBreakAtEntry = 0
"}}}
" }"}}}

"放置在Bundle的设置后，防止意外BUG
filetype plugin indent on
syntax on
